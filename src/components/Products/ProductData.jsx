import {useState, useEffect} from "react";
import "../../App.css";
import {db} from "../../Firebase_API/firebase-config.js";
import {
    collection,
    getDocs,
    addDoc,
    updateDoc,
    deleteDoc,
    doc
} from "firebase/firestore";

function ProductData() {
    const [Name,
        setName] = useState("");
    const [updateName,
        setUpdateName] = useState("");
    const [Price,
        setPrice] = useState(0);
    const [updatePrice,
        setupdatePrice] = useState(0);
    const [Quantity,
        setQuantity] = useState(0);
    const [updateQuantity,
        setUpdateQuantity] = useState(0);
    const [Product,
        setProduct] = useState([]);
    const ProductCollectionRef = collection(db, "Product");

    const createUser = async() => {
        await addDoc(ProductCollectionRef, {
            Product_Name: Name,
            Quantity: Number(Quantity),
            Price: Number(Price)
        });
        setName("");
        setPrice("")
        setQuantity("")
    };

    const updateProduct = async(id, Product_Name) => {
    
        const userDoc = doc(db, "Product", id);
        let newFields = {};
        if (updateName === '' && updatePrice === '' && updateQuantity === '') {
            newFields.Product_Name = Name;
            newFields.Price = Price;
            newFields.Quantity = Quantity;
        } else if (updateName !== '') {
            newFields.Product_Name = updateName
        } else if (updateQuantity !== '') {
            newFields.Quantity = updateQuantity;
        } else if(updatePrice !== ''){
            newFields.Price = updatePrice
        }
        else{
          newFields.Product_Name = Name;
          newFields.Price = Price;
          newFields.Quantity = Quantity;
         
          console.log('hii')
        }
        await updateDoc(userDoc, newFields);

    }

    const deleteUser = async(id) => {
        const userDoc = doc(db, "Product", id);
        await deleteDoc(userDoc);
    };

    useEffect(() => {
        const getProduct = async() => {
            const data = await getDocs(ProductCollectionRef);
            setProduct(data.docs.map((doc) => ({
                ...doc.data(),
                id: doc.id
            })));
        };

        getProduct();
    });

    return ( <> <div className="container w-25">

        <div className='card-body  mt-5'>
            <input
                className="form-control "
                placeholder="Enter Product Name"
                onChange={(event) => {
                setName(event.target.value);
            }}/>
            <input
                className="form-control mt-2"
                placeholder="Price "
                onChange={(event) => {
                setPrice(event.target.value);
            }}/>

            <input
                className="form-control mt-2"
                placeholder="Quantity"
                onChange={(event) => {
                setQuantity(event.target.value);
            }}/>

            <button className='btn btn-sm btn-success mt-2' onClick={createUser}>
                Add Product</button>
        </div>
    </div> 
    < div className = 'product' > 
    

        {Product.map((user) => {
            return (
                <div className='card-1 bg-light'>
                    <div className="card-body">
                        <h4>Product : {user.Product_Name}
                            <input
                                type='text'
                                className="form"
                                onChange={(event) => {
                                setUpdateName(event.target.value);
                            }}></input>
                        </h4>
                        <h4>Quantity : {user.Quantity}
                            <input
                                type='text'
                                className="form mt-2"
                                onChange={(event) => {
                                setUpdateQuantity(event.target.value);
                            }}></input>
                        </h4>
                        <h4>Price : {user.Price}
                            $<input
                                type='text'
                                className="form mt-2"
                                onChange={(event) => {
                    setupdatePrice(event.target.value);
                }}></input>
                        </h4>

                        <button
                            className='btn btn-sm btn-danger mt-2'
                            onClick={() => {
                            deleteUser(user.id);
                        }}>

                            Delete Product
                        </button>
                        <button
                            className='btn btn-sm btn-warning mt-2'
                            onClick={() => {
                            updateProduct(user.id, user.Name);
                        }}>
                            Update Product
                        </button>
                    </div>
                </div>

            );
        })}
    </div> 

    
    </>);
}

export default ProductData;