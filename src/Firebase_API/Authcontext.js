import React,{createContext} from 'react'
import {auth} from './Firebase'
import { onAuthStateChanged } from 'firebase/auth'
import { useState , useEffect} from 'react'

export let usercontext=createContext()
export let  Authcontext=({children})=>{
    let [user,setuser]= useState();
    
    useEffect(()=>{
        return onAuthStateChanged(auth,userinfo=>{
            if(userinfo && userinfo.emailVerified === true)
            {
           let Token=userinfo.accessToken;
           window.sessionStorage.setItem("Token",Token)
           setuser(userinfo)
            }
            else{
                window.sessionStorage.removeItem("Token")
                setuser(null)
            }
        })
    },[])

    return (
        <usercontext.Provider value={user}>
            {children}
        </usercontext.Provider>
        )
}

export default Authcontext;