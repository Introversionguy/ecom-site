
import { ToastContainer } from 'react-toastify';
import './App.css';
import Header from './components/Header';
import Home from './components/Home';
import Authcontext from './Firebase_API/Authcontext'
import Login from './components/Login';
import Signup from './components/Signup';
import {BrowserRouter,Route,Routes} from 'react-router-dom'
import AdminLogin from './components/Admin/AdminLogin';
import PhoneAuth from './PhoneAuth';


function App() {
  return (
    <div className="App">
      <ToastContainer pauseOnHover theme='dark'/>
      <Authcontext>
      <BrowserRouter>
      <Header/>
      <Routes>
        <Route path='/home' element={<Home/>}></Route>
        <Route path='/login' element={<Login/>}></Route>
        <Route path='/signup' element={<Signup/>}></Route>
        <Route path='/adminlogin' element={<AdminLogin/>}></Route>
       </Routes>
      </BrowserRouter>
      </Authcontext>

    {/* <PhoneAuth/> */}

   
    </div>
  );
}

export default App;
